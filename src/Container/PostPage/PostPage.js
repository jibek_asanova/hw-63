import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './PostPage.css';
import Spinner from "../../Components/UI/Spinner/Spinner";

const PostPage = ({match, history}) => {
    const [message, setMessage] = useState(null);
    const [loading, setLoading] = useState(false);


    const id = match.params.id;

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts/' + id + '.json');
            setMessage(response.data);
        };

        fetchData().catch(console.error);
    }, [id]);

    const deletePost = async () => {
        setLoading(true);

        try{
            await axiosApi.delete('/posts/' + id + '.json');
        }
        finally {
            setLoading(false);
            history.replace('/');
        }
    };

    const editPost = () => {
        history.replace('/posts/edit/' + id);
    }

    return message && (
        <>
            {loading ? <Spinner/> : <><h2>My post</h2>
                <div className="card">
                <h4 className="PostTitle">Title: {message.post.title}</h4>
                <p className="PostText">Message: {message.post.message}</p>
                <p className="PostDate">Created on: {message.post.date}</p>
                </div>
                <button type="button" className="btn btn-success" onClick={editPost}>Edit</button>
                <button type="button" className="btn btn-danger" onClick={deletePost}>Delete</button></>}
        </>
    );
};

export default PostPage;
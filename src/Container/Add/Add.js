import React, {useState} from 'react';
import './Add.css';
import axiosApi from "../../axiosApi";
import Spinner from "../../Components/UI/Spinner/Spinner";
import dayjs from "dayjs";

const Add = ({history}) => {
    const [post, setPost] = useState({
        title: '',
        message: '',
        date:''
    });

    const [loading, setLoading] = useState(false);

    const onInputChange = e => {
        const {name, value} = e.target;

        setPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    const createPost = async e => {
        e.preventDefault();

        setLoading(true);
        post.date = dayjs().format('DD.MM.YYYY HH:mm:ss');


        try {
            await axiosApi.post('/posts.json', {
                post
            });
            history.replace('/');
        } finally {
            setLoading(false);
        }

    }

    let form = (
        <form className="Form" onSubmit={createPost}>
            <input
                className="Input"
                type="text"
                name="title"
                placeholder="Title"
                value={post.title}
                onChange={onInputChange}
            />
            <textarea
                className="Input TextArea"
                name="message"
                placeholder="Description"
                value={post.message}
                onChange={onInputChange}
            />
            <button type="submit" className="btn btn-primary">Save</button>
        </form>
    );

    if(loading) {
        form = <Spinner/>;
    }

    return (
        <div className="AddPost">
            <h2>Add new post</h2>
            {form}
        </div>
    );
};

export default Add;
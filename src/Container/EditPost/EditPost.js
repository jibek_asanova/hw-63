import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";

const EditPost = ({match, history}) => {

    const [editPost, setEditPost] = useState([]);

    const onInputChange = e => {
        const {name, value} = e.target;

        setEditPost(prev => ({
            ...prev,
            [name]: value
        }))
    };

    useEffect(() => {
        const fetchData = async () => {
            const response = await axiosApi.get('/posts/' + match.params.id + '.json');
            setEditPost(response.data.post);
        };

        fetchData().catch(console.error);
    }, [match.params.id]);

    const changePost = (e) => {
        e.preventDefault();
        const fetchData = async () => {
           await axiosApi.put('/posts/' + match.params.id + '.json', {
                    post : {
                        title: editPost.title,
                        message: editPost.message,
                        date: editPost.date,
                    }
            });
           history.replace('/');
        };
        fetchData().catch(console.error);
    }


    return (
        <div>
            <form className="Form" onSubmit={changePost}>
                <h2>Edit post</h2>
                <input
                    className="Input"
                    type="text"
                    name="title"
                    placeholder={editPost.title}
                    value={editPost.title}
                    onChange={onInputChange}
                />
                <textarea
                    className="Input TextArea"
                    name="message"
                    placeholder={editPost.message}
                    value={editPost.message}
                    onChange={onInputChange}
                />
                <button type="submit" className="btn btn-primary">Save</button>
            </form>
        </div>
    )
}


export default EditPost;
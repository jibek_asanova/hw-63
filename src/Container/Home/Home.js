import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import Post from "../../Components/Post/Post";

const Home = () => {
    const [messages, setMessages] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            console.log('test');
            const response = await axiosApi.get('/posts.json');
            if(response.data !== null) {
                console.log(response);
                const response2 = Object.values(response.data);
                const keys = Object.keys(response.data);

                const messageArray = [];

                for (let i = 0; i < keys.length; i++) {
                    messageArray.push({
                        id: keys[i],
                        title: response2[i].post.title,
                        message: response2[i].post.message,
                        date: response2[i].post.date
                    })
                }
                setMessages(messageArray);
            }
        };

        fetchData().catch(console.error);
    }, []);



    return (
        <div>
            {messages.length ? messages.map(message => (
                    <Post
                        key={message.id}
                        message={message}
                    />
                )) : <p>Please add some posts</p>}


        </div>
    );
};

export default Home;
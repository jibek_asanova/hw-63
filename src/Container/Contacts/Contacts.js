import React from 'react';
import './Contacts.css';
import photo1 from '../../assets/photo-1.jpg';

const Contacts = () => {
    return (
        <div>
            <div className="about-us">
                <div className="about-us-img">
                    <img src={photo1} alt="" width="850" height="623"/>
                </div>
                <div className="about-us-info">
                    <div className="info-inner">
                        <h3>Contact us</h3>
                        <p><strong>Phone:</strong> +996 555 124 123</p>
                        <p><strong>Address:</strong> Sovetskaya street 1</p>
                        <p><strong>Email:</strong> blog@mail.ru</p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Contacts;
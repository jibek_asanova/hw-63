import React from 'react';
import './App.css';
import Add from "./Container/Add/Add";
import Navbar from "./Components/Navbar/Navbar";
import Home from "./Container/Home/Home";
import PostPage from "./Container/PostPage/PostPage";
import {BrowserRouter, Route, Switch} from "react-router-dom";
import EditPost from "./Container/EditPost/EditPost";
import About from "./Container/About/About";
import Contacts from "./Container/Contacts/Contacts";

const App = () => {
    return (
        <div className="App">
            <BrowserRouter>
                <Navbar/>
                <Switch>
                    <Route path="/" exact component={Home}/>
                    <Route path="/add" component={Add}/>
                    <Route path="/about" component={About}/>
                    <Route path="/contacts" component={Contacts}/>
                    <Route path="/posts/edit/:id" component={EditPost}/>
                    <Route path="/posts/:id" component={PostPage}/>
                </Switch>
            </BrowserRouter>
        </div>

    );
};

export default App;

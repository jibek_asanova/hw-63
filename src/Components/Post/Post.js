import React from 'react';
import './Post.css';
import {NavLink} from "react-router-dom";

const Post = ({message}) => {
    return (
        <div className="card"
        >
            <p>Created on: {message.date}</p>
            <p>Title: {message.title}</p>
            <NavLink to={'/posts/' + message.id} className="btn btn-light">Read More</NavLink>
        </div>
    );
};

export default Post;